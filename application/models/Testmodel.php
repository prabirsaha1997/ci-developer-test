<?php 
defined('BASEPATH') or exit('No direct script access is allowed');

class Testmodel extends CI_Model
{


    public function getOrderById($id)
	{
		$sql="Select orderNumber as order_id,orderDate as order_date,status from orders where orderNumber='".$id."'";    
        $query = $this->db->query($sql);
		if($query->result_array() > 0){
			return $query->result_array();
		}
		else{
			return false;
		}
	}

	 public function getOrderDetailsById($id)
	{
		$sql="Select a.productName as product,a.productLine as product_line,b.priceEach as unit_price,b.quantityOrdered as qty,b.orderLineNumber as line_total from products a,orderdetails b where a.productCode=b.productCode and b.orderNumber='".$id."'";    
        $query = $this->db->query($sql);
		if($query->result_array() > 0){
			return $query->result_array();
		}
		else{
			return false;
		}
	}

	public function getOrderTotalById($id)
	{
		$sql="SELECT SUM(orderLineNumber) AS bill_amount FROM orderdetails WHERE orderNumber='".$id."'";    
        $query = $this->db->query($sql);
		if($query->result_array() > 0){
			return $query->result_array();
		}
		else{
			return false;
		}
	}

	public function getCustomerById($id)
	{
		$sql="Select a.customerFirstName as first_name,a.customerLastName as last_name,a.phone,a.country from customers a,orders b where a.customerNumber=b.customerNumber and b.orderNumber='".$id."'";    
        $query = $this->db->query($sql);
		if($query->result_array() > 0){
			return $query->result_array();
		}
		else{
			return false;
		}
	}






}