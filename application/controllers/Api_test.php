<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/API_Controller.php';

class Api_Test extends API_Controller
{
    

    public function __construct()
    {
        parent::__construct();
         
          $this->load->model('Testmodel','c');
    }

    /**
     * demo method 
     *
     * @link [api/user/demo]
     * @method POST
     * @return Response|void
     */
    public function demo()
    {
        header("Access-Control-Allow-Origin: *");

        // API Configuration
        $this->_apiConfig([
            /**
             * By Default Request Method `GET`
             */
            'methods' => ['POST'], // 'GET', 'OPTIONS'

            /**
             * Number limit, type limit, time limit (last minute)
             */
            'limit' => [5, 'ip', 'everyday'],

            /**
             * type :: ['header', 'get', 'post']
             * key  :: ['table : Check Key in Database', 'key']
             */
            'key' => ['POST', $this->key() ], // type, {key}|table (by default)
        ]);
        
        // return data
        $this->api_return(
            [
                'status' => true,
                "result" => "Return API Response",
            ],
        200);
    }

    /**
     * Check API Key
     *
     * @return key|string
     */
    private function key()
    {
        // use database query for get valid key

        return 1452;
    }


    /**
     * login method 
     *
     * @link [api/user/login]
     * @method POST
     * @return Response|void
     */
    public function login()
    {
        header("Access-Control-Allow-Origin: *");

        // API Configuration
        $this->_apiConfig([
            'methods' => ['POST'],
        ]);

        // you user authentication code will go here, you can compare the user with the database or whatever
        $payload = [
            'id' => "8420852652",
            'password' => "test"
        ];

        // Load Authorization Library or Load in autoload config file
        $this->load->library('authorization_token');

        // generate a token
        $token = $this->authorization_token->generateToken($payload);

        // return data
        $this->api_return(
            [
                'status' => true,
                "result" => [
                    'token' => $token,
                ],
                
            ],
        200);
    }

    /**
     * view method
     *
     * @link [api/user/view]
     * @method POST
     * @return Response|void
     */
    public function view()
    {
        header("Access-Control-Allow-Origin: *");

        // API Configuration [Return Array: User Token Data]
        // $user_data = $this->_apiConfig([
        //     'methods' => ['POST'],
        //     'requireAuthorization' => true,
        // ]);

        $payload = [
            'id' => "8420852652",
            'password' => "test"
        ];

        // return data
        $this->api_return(
            [
                'status' => true,
                "result" => [
                    'user_data' => $payload
                ],
            ],
        200);
    }


     public function order($id)
    {
        header("Access-Control-Allow-Origin: *");

      
        // $payload=[];
        $payload = $this->c->getOrderById($id);
        $orderdetails = $this->c->getOrderDetailsById($id);
        $orderSum = $this->c->getOrderTotalById($id);
        $customer= $this->c->getCustomerById($id);
        if(!empty($orderdetails)){
        $payload[0]['order_details']=$orderdetails;
        }
        if(isset($orderSum[0]['bill_amount'])){
        $payload[0]['bill_amount']=$orderSum[0]['bill_amount'];
        }
        if(!empty($customer)){
        $payload[0]['customer']=$customer[0];
        }

        if(!empty($payload)){
            $status=200;
        }else{
            $status=400;
        }
        
       // array_push($payload[0]['order_details'],$payloads);

        // return data
        $this->api_return(
            [
                'status' => $status,
                "result" => $payload
            ],
        200);
    }
}